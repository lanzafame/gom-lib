package lib

import "fmt"

// Hello says hello to the name provided.
func Hello(name string) {
	fmt.Println("Hello " + name)
}

// Goodbye says goodbye to the name provided.
func Goodbye(name string) {
	fmt.Println("Goodbye " + name)
}

// Exclamation exclaims the name provided.
func Exclamation(name string) {
	fmt.Println(name + "!")
}
